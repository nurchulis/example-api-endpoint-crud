<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;

class UserController extends Controller
{
    function post(Request $request)
    {
        $user = new User;
        $user->username = $request->username;
        $user->phone = $request->phone;  
        
        $user->save();
        return response()->json(
            [
                "message" => "Success",
                "data" => $user
            ]
        );
    }
    function get()
    {
        $data =  User::all();

        return response()->json(
            [
                "message" => "Success",
                "data" => $data
            ]
        );
    }
    function getById($id)
    {
        $data =  User::where('id', $id)->get();

        return response()->json(
            [
                "message" => "Success",
                "data" => $data
            ]
        );
    }
    function put($id, Request $request)
    {
        $user = User::where('id', $id)->first();
        if ($user){
            $user->username = $request->username ? $request->username : $user->username;
            $user->phone = $request->phone ? $request->phone : $user->phone;  

            $user->save();
            return response()->json(
                [
                    "message" => "PUT Method Success ",
                    "data" => $user
                ]
            );
        } 
        return response()->json(
            [
                "message" => "Product with id : " . $id . " Not Found"
            ], 400
        );

    }
    function delete($id)
    {
        $user = User::where('id', $id)->first();
        if($user){
            $user->delete();
            return response()->json(
                [
                    "message" => "Delete User Success. Id : " . $id . " was deleted"

                ]
            );
        }
        return response()->json(
            [
                "message" => "Delete User Message " . $id . " not found"
            ]
        );
    }
}
